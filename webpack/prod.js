// production config
const {resolve} = require('path');

module.exports = {
  mode: 'production',
  entry: [
    'babel-polyfill',
    './index.tsx'],
  output: {
    chunkFilename: '[name].[chunkhash].js',
    path: resolve(__dirname, '../dist'),
    publicPath: 'dist',
  },
  plugins: [],
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: Infinity,
      minSize: 0,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name(module) {
            // get the name. E.g. node_modules/packageName/not/this/part.js
            // or node_modules/packageName
            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

            // npm package names are URL-safe, but some servers don't like @ symbols
            return `npm.${packageName.replace('@', '')}`;
          },
        },
      },
    },
  }
};