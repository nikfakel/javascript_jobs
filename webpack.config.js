// shared config (dev and prod)
const {resolve} = require('path');
const {CheckerPlugin} = require('awesome-typescript-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const merge = require('webpack-merge');
const devConfig = require('./webpack/dev.js');
const prodConfig = require('./webpack/prod.js');

module.exports = env => {
  const config = env && env.production ? prodConfig : devConfig;

  const options = merge(config, {
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx'],
      alias: {
        components: resolve(__dirname, "./src/components"),
        modules: resolve(__dirname, "./src/modules"),
        services: resolve(__dirname, "./src/services"),
        store: resolve(__dirname, "./src/store"),
        styled: resolve(__dirname, "./src/styled-components")
      },
    },
    context: resolve(__dirname, 'src'),
    module: {
      rules: [
        {
          test: /\.js$/,
          use: ['babel-loader', 'source-map-loader'],
          exclude: /node_modules/,
        },
        {
          test: /\.tsx?$/,
          use: ['babel-loader', 'awesome-typescript-loader'],
        },
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: ['css-loader', 'sass-loader']
          })
        }
      ]
    },
    plugins: [
      new CheckerPlugin(),
      new HtmlWebpackPlugin({
        template: 'index.html.ejs',
        filename: resolve(__dirname, './dist/index.html')
      }),
      new ExtractTextPlugin('style.css')
    ],
    performance: {
      hints: false,
    }
  });


  return {
    ...options
  }
};

