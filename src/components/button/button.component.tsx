import * as React from "react";
import styled from "../../styled-components";

export const Button = styled.button`
  border: 1px solid #ccc;
  padding: 5px 10px;
  font-size: 20px;
`;
