import * as React from "react";
import styled from "styled";

export const PageLoader = () => {
  return (
    <PageLoaderWrapper><h3>Page is loading</h3></PageLoaderWrapper>
  );
};

const PageLoaderWrapper = styled.div`

`;
