import styled from "styled";

const COLUMNS = 12;
const GUTTER = "20px";

/*tslint:disable*/
const breakpoints = {
  xs: 480,
  sm: 768,
  md: 992,
  qw: 1200,
  lg: 1440,
};
/*tslint:enable*/

const medias = {};
for (const key in breakpoints) {
  if (breakpoints.hasOwnProperty(key)) {
    medias[key] = `@media (min-width: ${breakpoints[key]}`  + "px)";
  }
}

export const Container = styled.div`
  max-width: 1320px;
  width: 100%;
  padding: 0 ${GUTTER}px;
  margin: 0 auto;
`;

export const Row = styled.div`
  margin: 0 -${GUTTER}px;
  display: flex;
  flex-wrap: wrap;
`;

export const Col = styled("div")<{xs?: number, sm?: number, qw?: number, md?: number, lg?: number}>`
  padding: 0 ${GUTTER}px;
  flex-shrink: 0;
  flex-grow: 0;
  position: relative;
  width: 100%;

  ${(props) => props.xs && "flex-basis:" + props.xs * 100 / COLUMNS + "%"};
  ${(props) => props.xs && "max-width:" + props.xs * 100 / COLUMNS + "%"};

  @media (min-width: ${breakpoints.xs}px) {
    ${(props) => props.sm && "flex-basis:" + props.sm * 100 / COLUMNS + "%"};
    ${(props) => props.sm && "max-width:" + props.sm * 100 / COLUMNS + "%"};
  }

  @media (min-width: ${breakpoints.sm}px) {
    ${(props) => props.qw && "flex-basis:" + props.qw * 100 / COLUMNS + "%"};
    ${(props) => props.qw && "max-width:" + props.qw * 100 / COLUMNS + "%"};
  }

  @media (min-width: ${breakpoints.qw}px) {
    ${(props) => props.md && "flex-basis:" + props.md * 100 / COLUMNS + "%"};
    ${(props) => props.md && "max-width:" + props.md * 100 / COLUMNS + "%"};
  }

  @media (min-width: ${breakpoints.md}px) {
    ${(props) => props.lg && "flex-basis:" + props.lg * 100 / COLUMNS + "%"};
    ${(props) => props.lg && "max-width:" + props.lg * 100 / COLUMNS + "%"};
  }

`;
