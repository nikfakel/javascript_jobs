import * as React from "react";
import {useState} from "react";
import {postType} from "services/constants";
import styled from "styled";

interface IProps {
  initialPostType: postType;
  handlePostTypeChange: (postType: postType) => void;
}

export const Type = (props: IProps) => {
  const [currentPostType, setCurentPostType] = useState(props.initialPostType);

  const handlePostTypeChange = (postType: postType) => {
    props.handlePostTypeChange(postType);
    setCurentPostType(postType);
  };

  const renderItem = (postType: postType) => (
    <TypeItem key={postType}>
      <Item
        type="radio"
        name="postType"
        value={postType}
        id={postType}
        checked={postType === currentPostType}
        onChange={handlePostTypeChange.bind(this, postType)}
      />
      <Label
        htmlFor={postType}
      >{postType}</Label>
    </TypeItem>
  );

  return (
    <TypeWrapper>
      <Header>Choose type</Header>
      <ChooseItem>
        {Object.keys(postType).map((key) => (
          renderItem(postType[key])
        ))}
      </ChooseItem>
    </TypeWrapper>
  );
};

const TypeWrapper = styled.div`

`;

const Header = styled.div`

`;

const ChooseItem = styled.div`

`;

const Item = styled.input`

`;

const TypeItem = styled.div`

`;

const Label = styled.label`

`;
