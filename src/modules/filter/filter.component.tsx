import {Button} from "components/button/button.component";
import * as React from "react";
import {useContext} from "react";
import {postType} from "services/constants";
import {FilterContext} from "store/filter";
import {PostsContext} from "store/posts";
import styled from "styled";
import {Keywords} from "./keywords/keywords.component";
import {Type} from "./type/type.component";

export function Filter() {
  const postsStore = useContext(PostsContext);
  const filtersStore = useContext(FilterContext);

  let currentPostType = filtersStore.postType;
  let currentKeywords = filtersStore.keywords;

  const handlePostTypeChange = (postType: postType) => {
    currentPostType = postType;
  };

  const handleKeywordsChange = (keywordsArray) => {
    currentKeywords = keywordsArray;
  };

  const applyFilters = () => {
    const payload = {
      keywords: currentKeywords,
      postType: currentPostType,
    };

    postsStore.fetchPosts(payload);
    filtersStore.saveFilterState(payload);
  };

  return (
    <FilterWrapper>
      <Header>Filter</Header>
      <Fields>
        <FieldWrapper>
          <Keywords
            handleKeywordsChange={handleKeywordsChange}
            applyFilters={applyFilters}
            initialKeywords={currentKeywords}/>
          <Type
            handlePostTypeChange={handlePostTypeChange}
            initialPostType={currentPostType}/>
        </FieldWrapper>
      </Fields>
      <SendButton>
        <Button onClick={() => applyFilters()}>Search</Button>
      </SendButton>
    </FilterWrapper>
  );
}

const FilterWrapper = styled.div`
  padding: 20px;
`;

const Header = styled.div`
  margin-bottom: 20px;
`;

const Fields = styled.div`

`;

const FieldWrapper = styled.div`

`;

const SendButton = styled.div`

`;
