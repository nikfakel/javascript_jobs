import {observer} from "mobx-react-lite";
import * as React from "react";
import styled from "styled";

interface IProps {
  handleKeywordsChange: (keywords) => void;
  initialKeywords: string[];
  applyFilters: () => void;
}

export const Keywords = observer((props: IProps) => {
  const [value, setValue] = React.useState(props.initialKeywords.join(" "));

  const handleKeywordsChange = (e) => {
    setValue(e.target.value);
    const keywordsArray = e.target.value.split(" ");
    props.handleKeywordsChange(keywordsArray);
  };

  const handleEnterKey = (e) => {
    if (e.key === "Enter") {
      props.applyFilters();
    }
  };

  return (
    <KeywordsWrapper>
      <Header>Keywords</Header>
      <Field>
        <Input
          type="text"
          value={value}
          onChange={(e) => handleKeywordsChange(e)}
          onKeyPress={handleEnterKey}
        />
      </Field>
    </KeywordsWrapper>
  );
});

const KeywordsWrapper = styled.div`

`;

const Header = styled.div`
  font-weight: bold;
`;

const Field = styled.div`

`;

const Input = styled.input`

`;
