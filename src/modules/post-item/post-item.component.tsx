import * as React from "react";
import {Link} from "react-router-dom";
import {convertUnixToLocalDate} from "services/datesUtils";
import styled from "styled";

interface IProps {
  post: any;
}

export const PostItem = (props: IProps) => {
  const {post} = props;

  const renderContent = (content) => (
    <>
      {content.split("\n").map((line, key) =>
        <p key={key}>{line}</p>,
      )}
    </>
  );

  return (
    <ComponentWrapper>
      <Link to={`/post/${post.uuid}`}>Link</Link>
      <Header>{convertUnixToLocalDate(post.date)}</Header>
      <Tags>
        {post.tags && post.tags.map((tag, index) =>
          <Tag key={index}>{tag}</Tag>,
        )}
      </Tags>
      <Description>
        {renderContent(post.content)}
      </Description>
    </ComponentWrapper>
  );
};

const ComponentWrapper = styled.div`
  margin-bottom: 20px;
  border: 1px solid #ccc;
  padding: 20px;
`;

const Header = styled.div`
  font-weight: bold;
  font-size: 16px;
  margin-bottom: 10px;
`;

const Description = styled.div`

`;

const Tags = styled.div`
  display: flex;
`;

const Tag = styled.div`
  padding: 5px;
  background: #ddd;
  margin-right: 5px;
`;
