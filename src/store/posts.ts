import {cast, clone, flow, types} from "mobx-state-tree";
import * as React from "react";
import {postType, state} from "services/constants";
import * as transport from "services/transport";

const PostModel = types
  .model("PostModel", {
    content: types.string,
    date: types.Date,
    tags: types.array(types.string),
    uuid: types.string,
  });

const Post = types
  .model("PostStore", {
    post: types.maybeNull(PostModel),
    state: types.enumeration<state>("Post state", Object.values(state)),
  })
  .actions((self) => ({
    fetchPost: flow(function* fetchPost({uuid}) {
      self.post = null;
      self.state = state.pending;
      // check if post already is in exists posts
      const postIndex = postsStore.posts.findIndex((post) => post.uuid === uuid);
      if (postIndex > 0) {
        self.post = clone(postsStore.posts[postIndex]);
        self.state = state.success;
      } else {
        try {
          self.post = yield transport.getPost({uuid});
          self.state = state.success;
        } catch (err) {
          console.log(err);
        }
      }
    }),
  }));

interface IGetPostsOptions {
  dateTo?: Date;
  dateFrom?: Date;
  keywords?: string[];
  limit?: number;
  postType?: postType;
}

export const Posts = types.model("PostsModel", {
  posts: types.array(PostModel),
  state: types.enumeration<state>("Posts state", Object.values(state)),
})
  .actions((self) => ({
    checkAndUpdateAllPosts: flow(function* checkAndUpdateAllPosts() {
      try {
        const answer = yield transport.checkAndUpdateAllPosts();
        console.log(answer);
      } catch (err) {
        console.log(err);
      }
    }),
    fetchPosts: flow(function* fetchPosts({dateTo, dateFrom, keywords, limit, postType}: IGetPostsOptions) {
      self.posts = cast([]);
      self.state = state.pending;
      try {
        self.posts = yield transport.getPosts({dateTo, dateFrom, keywords, limit, postType});
        self.state = state.success;
      } catch (error) {
        console.log(error);
        self.state = state.error;
      }
    }),
  }));

export const PostContext = React.createContext(Post.create({
  post: undefined,
  state: state.pending,
}));

export const postsStore = Posts.create({posts: cast([]), state: state.pending});
export const PostsContext = React.createContext(postsStore);
