import {cast, getSnapshot, types} from "mobx-state-tree";
import * as React from "react";
import {postType} from "services/constants";

const localStorageName = "app-data-saving-storage";

const Filter = types
  .model("FilterModel", {
    keywords: types.array(types.string),
    postType: types.enumeration("PostType", [
      postType.vacancy,
      postType.resume,
      postType.all,
    ]),
  })
  .actions((self) => ({
    saveFilterState: ({keywords, postType}: {keywords?: string[], postType?: postType}) => {
      self.keywords = cast(keywords);
      self.postType = postType;
      const snapshot = getSnapshot(self);
      localStorage.setItem(localStorageName, JSON.stringify(snapshot));
    },
  }));

const getInitialState = () => {
  const localStorageValue = JSON.parse(localStorage.getItem(localStorageName));
  const initialState = {
    keywords: [],
    postType: postType.vacancy,
  };

  return localStorageValue ? localStorageValue : initialState;
};

export const FilterContext = React.createContext(Filter.create(getInitialState()));
