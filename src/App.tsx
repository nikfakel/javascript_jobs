import {Col, Container, Row} from "components/grid/grid.component";
import {PageLoader} from "components/page-loader/page-loader.component";
import {Filter} from "modules/filter/filter.component";
import * as React from "react";
import {Link, Route, Switch} from "react-router-dom";
import styled from "./styled-components";

const LazyIndexPage = (
  React.lazy(() => (
    import("./scenes/index/index.component")
  ))
);

const LazyPostPage = (
  React.lazy(() => (
    import("./scenes/post-page/post-page.component")
  ))
);

const LazyArchivePage = (
  React.lazy(() => (
    import("./scenes/archive/archive.component")
  ))
);

const App = () => {
  return (
    <AppWrapper>
      <Container>
        <Row>
          <Col sm={4}>
            <Filter/>
            <Link to="/">Home</Link>
            <Link to="/archive">Archive</Link>
          </Col>
          <Col sm={8}>
            <React.Suspense fallback={<PageLoader/>}>
              <Switch>
                <Route exact path="/" render={() => <LazyIndexPage/>}/>
                <Route path="/post/:id" render={(props) => <LazyPostPage {...props}/>}/>
                <Route exact path="/archive" render={() => <LazyArchivePage/>}/>
              </Switch>
            </React.Suspense>
          </Col>
        </Row>
      </Container>
    </AppWrapper>
  );
};

export default App;

const AppWrapper = styled.div`
  color: #fff;
  background: #222;
  min-height: 100vh;
  font-family: sans-serif;
`;
