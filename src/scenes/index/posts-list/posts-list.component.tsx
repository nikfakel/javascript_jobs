import {Button} from "components/button/button.component";
import {observer} from "mobx-react-lite";
import {PostItem} from "modules/post-item/post-item.component";
import * as React from "react";
import {useContext, useEffect} from "react";
import {FilterContext} from "store/filter";
import {PostsContext} from "store/posts";
import styled from "styled";

export const ItemsList = observer(() => {
  const postsStore = useContext(PostsContext);
  const filterStore = useContext(FilterContext);

  useEffect(() => {
    postsStore.fetchPosts({
      keywords: filterStore.keywords,
      postType: filterStore.postType,
    });
  }, []);

  return (
    <ItemsListWrapper>
      <LoadButton>
        <Button onClick={postsStore.checkAndUpdateAllPosts}>Remove and parse all posts</Button>
      </LoadButton>
      <Loader><h3>Is loading - {postsStore.state}</h3></Loader>
      <Items>
        {postsStore && postsStore.posts && postsStore.posts.map((post) =>
          <PostItem post={post} key={post.uuid}/>,
        )}
      </Items>
    </ItemsListWrapper>
  );
});

const ItemsListWrapper = styled.div`

`;

const Items = styled.div`

`;

const Loader = styled.div`

`;

const LoadButton = styled.div`

`;
