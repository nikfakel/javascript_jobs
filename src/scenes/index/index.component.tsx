import * as React from "react";
import {ItemsList} from "./posts-list/posts-list.component";

const IndexPage = () => {

  return (
    <ItemsList/>
  );
};

export default IndexPage;
