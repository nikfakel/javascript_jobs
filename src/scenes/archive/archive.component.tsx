import {observer} from "mobx-react-lite";
import * as React from "react";
import {useContext, useEffect} from "react";
import { XYFrame } from "semiotic";
import {postType} from "services/constants";
import {PostsContext} from "store/posts";
import styled from "styled-components";
import "../../scss/d3styles.scss"; // TODO: separate loading for css files

const ArchivePage = observer(() => {
  const postsStore = useContext(PostsContext);
  useEffect(() => {
    postsStore.fetchPosts({
      dateTo: new Date(),
      keywords: ["React"],
      postType: postType.vacancy,
    });
  }, []);

  return (
    <>
      <Header>Archive</Header>
      <ArchiveList>
        <XYFrame
          points={[{price: 10.25, size: 150}, {price: 20.25, size: 120}]}
          pointStyle={{ fill: "blue" }}
          xAccessor={"price"}
          yAccessor={"size"}
        />
      </ArchiveList>
    </>
  );
});

export default ArchivePage;

const Header = styled.div`

`;

const ArchiveList = styled.div`
  background: #fff;
`;
