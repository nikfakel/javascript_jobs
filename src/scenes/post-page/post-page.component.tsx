import {observer} from "mobx-react-lite";
import {PostItem} from "modules/post-item/post-item.component";
import * as React from "react";
import {useContext, useEffect} from "react";
import {PostContext} from "store/posts";
import styled from "styled";

export const PostPage = observer((props: any) => {
  const postStore = useContext(PostContext);
  const {post} = postStore;
  const uuid = props && props.match && props.match.params && props.match.params.id && props.match.params.id;

  let postsFetched = false;

  useEffect(() => {
    postStore.fetchPost({uuid});
    postsFetched = true;
  }, [postsFetched]);

  return (
    <PostPageWrapper>
        {post &&
          <PostItem post={post}/>
        }
    </PostPageWrapper>
  );
});

export default PostPage;

const PostPageWrapper = styled.div`

`;
