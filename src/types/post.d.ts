type Tag = string;

declare interface IPost {
  uuid: string;
  index: number;
  tags: Tag[];
  content: string;
  date: number;
}
