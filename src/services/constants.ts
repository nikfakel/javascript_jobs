export const CONFIG = {
  API_ROOT: "http://localhost:5000",
};

export enum ENDPOINTS {
  getPosts = "get-posts",
  getPost = "get-post",
  checkAndUpdateAllPosts = "dev-check-and-update-all-posts",
}

export enum METHODS {
  GET = "GET",
  POST = "POST",
}

export enum state {
  pending = "Pending",
  success = "Success",
  error = "Error",
}

export enum postType {
  vacancy = "Vacancy",
  resume = "Resume",
  all = "All",
}
