import {API} from "services/API";
import {ENDPOINTS, METHODS, postType} from "services/constants";
import {dateToUnixTimestamp} from "services/datesUtils";

interface IGetPosts {
  dateFrom?: Date;
  dateTo: Date;
  keywords?: string[];
  limit?: number;
  postType?: postType;
}

export const getPosts = async ({dateTo, dateFrom, keywords, limit, postType}: IGetPosts): Promise<IPost[]> => {
  const body: {dateFrom?: number, dateTo?: number, keywords?: string[], limit?: number, postType?: postType} = {};

  if (dateTo) {
    body.dateTo = dateToUnixTimestamp(dateTo);
  }

  if (dateFrom) {
    body.dateFrom = dateToUnixTimestamp(dateFrom);
  }

  if (keywords) {
    body.keywords = keywords;
  }

  if (postType) {
    body.postType = postType;
  }

  if (limit) {
    body.limit = limit;
  }

  try {
    const response = await API.request(ENDPOINTS.getPosts, METHODS.POST, body);
    return response.posts;
  } catch (err) {
    console.log(err);
  }
};

export const getPost = async ({uuid}) => {
  try {
    const response = await API.request(ENDPOINTS.getPost, METHODS.POST, {
      uuid,
    });
    return response.post;
  } catch (err) {
    console.log(err);
  }
};

export const checkAndUpdateAllPosts = async () => {
  try {
    const response = await API.request(ENDPOINTS.checkAndUpdateAllPosts, METHODS.POST);
    return response.answer;
  } catch (err) {
    console.log(err);
  }
};
