export function convertUnixToLocalDate(unixDate) {
  const date = new Date(unixDate * 1000);
  /*tslint:disable*/
  const options = {
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
  };
  /*tslint:enable*/

  return date.toLocaleString("ru", options);
}

export const dateToUnixTimestamp = (date) => {
  const d = new Date(date);
  return Math.round(d.getTime() / 1000);
};
