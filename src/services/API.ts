import "whatwg-fetch";
import {CONFIG, ENDPOINTS, METHODS} from "./constants";

interface IOptions {
  body?: string;
  method: METHODS;
  headers: {
    "Content-Type": string,
  };
}

export class API {
  public static request = (endpoint: ENDPOINTS, method: METHODS = METHODS.GET, body?: any) => {
    const options: IOptions = {
      headers: {
        "Content-Type": "application/json",
      },
      method,
    };

    if (body) {
      options.body = JSON.stringify(body);
    }

    return fetch(`${CONFIG.API_ROOT}/${endpoint}`, options).then((response) => {

      if (response.status === 204) {
        return null;
      }

      if (response.status >= 400) {
        return response.json().then((err) => {
          throw err;
        });
      }

      return response.json();
    }).catch((err) => {
      throw err;
    });
  }
}
