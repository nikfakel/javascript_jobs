import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./App";

import {AppContainer} from "react-hot-loader";
import {BrowserRouter} from "react-router-dom";
import * as serviceWorker from "./serviceWorker";

const rootEl = document.getElementById("root");

ReactDOM.render(
  <AppContainer>
    <BrowserRouter>
        <App/>
    </BrowserRouter>
  </AppContainer>,
  rootEl,
);

declare let module: { hot };

if (module.hot) {
    module.hot.accept(<App/>, () => {

    ReactDOM.render(
      <AppContainer>
        <BrowserRouter>
          <App/>
        </BrowserRouter>
      </AppContainer>,
      rootEl,
    );
  });
}

serviceWorker.unregister();
